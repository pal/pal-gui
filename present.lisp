(in-package :pal-gui)


(defmethod present (object (g widget) width height)
  (with-blend (:color *text-color*)
    (draw-text (format nil "~a" object) (v (vx *text-offset*)
                                           (- (truncate height 2) (truncate (get-font-height *gui-font*) 2) 1))
               *gui-font*)))



(defmethod present ((image image) (g widget) width height)
  (draw-image image (v 0 0) :scale (min (/ height (image-height image)) (/ width (image-width image)))))



(defmethod present ((s (eql :up-arrow)) (g widget) width height)
  (draw-polygon (list (v 3 (- height 3))
                      (v (/ width 2) 3)
                      (v (- width 3) (- height 3)))
                (color-r *text-color*) (color-g *text-color*) (color-b *text-color*)(color-a *text-color*) :smoothp t))


(defmethod present ((s (eql :down-arrow)) (g widget) width height)
  (draw-polygon (list (v 3 3)
                      (v (/ width 2) (- height 3))
                      (v (- width 3) 3))
                (color-r *text-color*) (color-g *text-color*) (color-b *text-color*)(color-a *text-color*) :smoothp t))


(defmethod present ((s (eql :right-arrow)) (g widget) width height)
  (draw-polygon (list (v 3 3)
                      (v (- width 3) (/ height 2))
                      (v 3 (- height 3)))
                (color-r *text-color*) (color-g *text-color*) (color-b *text-color*)(color-a *text-color*) :smoothp t))


(defmethod present ((s (eql :left-arrow)) (g widget) width height)
  (draw-polygon (list (v (- width 3) 3)
                      (v 3 (/ height 2))
                      (v (- width 3) (- height 3)))
                (color-r *text-color*) (color-g *text-color*) (color-b *text-color*)(color-a *text-color*) :smoothp t))


(defmethod present ((s (eql :box)) (g widget) width height)
  (draw-rectangle (v 3 3) (- width 6) (- height 6) (color-r *text-color*) (color-g *text-color*) (color-b *text-color*)(color-a *text-color*) :smoothp t))


(defmethod present ((s (eql :circle)) (g widget) width height)
  (draw-circle (v (/ width 2) (/ height 2)) (/ (min width height) pi) (color-r *text-color*) (color-g *text-color*) (color-b *text-color*)(color-a *text-color*) :smoothp t))
