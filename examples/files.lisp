(in-package :pal-gui)


;; a Toy file selector

(defclass file-widget (v-box)
  ((list-widget :accessor list-widget-of)
   (text-widget :accessor text-widget-of)
   (select :accessor select-of))
  (:default-initargs :gap 2))

(defmethod initialize-instance :after ((g file-widget) &key &allow-other-keys)
  (setf (list-widget-of g) (make-instance 'list-widget :parent g :on-select (lambda (lg)
                                                                              (setf (text-of (text-widget-of g))
                                                                                    (selected-of lg)))))
  (let ((hbox (make-instance 'h-box :parent g :gap 2 :y-expand-p nil)))
    (setf (text-widget-of g) (make-instance 'text-widget :parent hbox))
    (setf (select-of g) (make-instance 'button :x-expand-p nil :width *m* :value :box :parent hbox)))
  (update-view g))

(defmethod update-view ((g file-widget))
  (setf (items-of (list-widget-of g)) (mapcar (lambda (f)
                                                (if (pathname-name f)
                                                    (pathname-name f)
                                                    (concatenate 'string (first (last (pathname-directory f))) "/")))
                                              (directory "*"))))


(defun test ()
  (with-pal ()
    (let* ((window (make-instance 'window :pos (v 200 200) :width 300 :height 200))

           (hbox (make-instance 'file-widget :parent window :label "Choose")))

      (event-loop ()
        (clear-screen (color 150 150 150))))))

;; (test)