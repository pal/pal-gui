
(in-package #:asdf)

(defsystem pal-gui
  :description "Pixel Art Library GUI"
  :author "Tomi Neste"
  :license "MIT"
  :components
  ((:file "gob"
          :depends-on ("package"))
   (:file "widgets"
          :depends-on ("gob"))
   (:file "gui"
          :depends-on ("gob" "widgets"))
   (:file "present"
          :depends-on ("widgets"))
   (:file "package"))
  :depends-on ("pal"))


