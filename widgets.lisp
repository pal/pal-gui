(in-package :pal-gui)

;; (declaim (optimize (speed 3)))


(defvar *window-color* nil)
(defvar *widget-color* nil)
(defvar *text-color* nil)
(defvar *paper-color* nil)
(defvar *tooltip-delay* nil)
(defvar *widget-enter-time* nil)
(defvar *m* nil)
(defvar *text-offset* nil)
(defvar *gui-font* nil)




(defun get-text-bounds (string)
  (let ((fh (get-font-height *gui-font*)))
    (values (max (truncate (* 1.5 fh)) (+ (get-text-size string *gui-font*) fh))
            (truncate (* fh 1.5)))))


(defun draw-frame (pos width height color &key style (border 1) (fill t))
  (let ((pos (v-floor pos))
        (width (truncate width))
        (height (truncate height))
        (r (color-r color))
        (g (color-g color))
        (b (color-b color))
        (a (color-a color)))
    (when fill
      (draw-rectangle pos width height r g b a))
    (when (> border 0)
      (draw-rectangle (v- pos (v border border)) (+ width (* 2 border)) (+ height (* 2 border)) 0 0 0 a :fill nil))
    (case style
      (:raised
       (draw-line pos (v+ pos (v width 0)) 255 255 255 128)
       (draw-line pos (v+ pos (v 0 height)) 255 255 255 128)
       (draw-line (v+ pos (v width height)) (v+ pos (v width 0)) 0 0 0 128)
       (draw-line (v+ pos (v width height)) (v+ pos (v 0 height)) 0 0 0 128))
      (:sunken
       (draw-line pos (v+ pos (v width 0)) 0 0 0 128)
       (draw-line pos (v+ pos (v 0 height)) 0 0 0 128)
       (draw-line (v+ pos (v width height)) (v+ pos (v width 0)) 255 255 255 128)
       (draw-line (v+ pos (v width height)) (v+ pos (v 0 height)) 255 255 255 128)))))




(defgeneric present (object gob width height))



(defclass widget (gob)
  ((tooltip :accessor tooltip-of :initarg :tooltip :initform nil)
   (on-drag :accessor on-drag-of :initarg :on-drag :initform (lambda (widget pos d) (declare (ignore widget pos d)) nil))
   (on-select :accessor on-select-of :initarg :on-select :initform (lambda (widget) (declare (ignore widget)) nil))
   (on-over :accessor on-over-of :initarg :on-over :initform (lambda (widget) (declare (ignore widget)) nil))
   (on-repaint :accessor on-repaint-of :initarg :on-repaint :initform (lambda (widget) (declare (ignore widget)) nil))
   (on-button-down :accessor on-button-down-of :initarg :on-button-down :initform (lambda (widget pos) (declare (ignore widget pos)) nil))
   (on-button-up :accessor on-button-up-of :initarg :on-button-up :initform (lambda (widget pos) (declare (ignore widget pos)) nil))
   (on-key-down :accessor on-key-down-of :initarg :on-key-down :initform (lambda (widget char) (declare (ignore widget char)) nil))
   (on-enter :accessor on-enter-of :initarg :on-enter :initform (lambda (widget) (declare (ignore widget)) nil))
   (on-leave :accessor on-leave-of :initarg :on-leave :initform (lambda (widget) (declare (ignore widget)) nil)))
  (:default-initargs :width *m* :height *m*))


(defmethod on-inspect ((g gob))
  (message g))

(defmethod on-drag :around ((g widget) pos d)
  (unless (funcall (on-drag-of g) g pos d)
    (call-next-method)))

(defmethod on-select :around ((g widget))
  (unless (funcall (on-select-of g) g)
    (call-next-method)))

(defmethod repaint :around ((g widget))
  (unless (funcall (on-repaint-of g) g)
    (call-next-method)))

(defmethod on-over :around ((g widget))
  (when (and *widget-enter-time* (tooltip-of g) (> (- (get-universal-time) *widget-enter-time*) *tooltip-delay*))
    (setf *widget-enter-time* nil)
    (make-instance 'tooltip :text (tooltip-of g) :host g))
  (unless (funcall (on-over-of g) g)
    (call-next-method)))

(defmethod on-button-down :around ((g widget) pos)
  (unless (funcall (on-button-down-of g) g pos)
    (call-next-method)))

(defmethod on-button-up :around ((g widget) pos)
  (unless (funcall (on-button-up-of g) g pos)
    (call-next-method)))

(defmethod on-key-down :around ((g widget) char)
  (unless (funcall (on-key-down-of g) g char)
    (call-next-method)))

(defmethod on-enter :around ((g widget))
  (setf *widget-enter-time* (get-universal-time))
  (unless (funcall (on-enter-of g) g)
    (call-next-method)))

(defmethod on-leave :around ((g widget))
  (unless (funcall (on-leave-of g) g)
    (call-next-method)))

(defmethod highlight ((g widget))
  (draw-rectangle (v 0 0) (width-of g) (height-of g) 255 255 255 32))




(defclass box (widget)
  ((label :accessor label-of :initform nil :initarg :label))
  (:default-initargs :activep nil :x-expand-p t :y-expand-p t))

(defmethod repaint ((g box))
  (when (label-of g)
    (with-accessors ((width width-of) (height height-of) (label label-of)) g

      (draw-line (v 0 0) (v 0 height) 0 0 0 160)
      (draw-line (v width 0) (v width height) 0 0 0 160)
      (draw-line (v 0 height) (v width height) 0 0 0 160)

      (draw-line (v 0 0) (v (vx *text-offset*) 0) 0 0 0 160)
      (draw-line (v (- (get-text-bounds label) (vx *text-offset*)) 0) (v width 0) 0 0 0 160)

      (with-blend (:color *text-color*)
        (draw-text label (v- *text-offset* (v 0 (truncate *m* 2))) *gui-font*)))))



(defclass v-box (box v-packing)
  ()
  (:default-initargs :x-pad 0 :y-pad 0 :gap (truncate *m* 3)))

(defmethod initialize-instance :after ((g v-box) &key label)
  (when label
    (setf (y-pad-of g) (truncate *m* 2)
          (x-pad-of g) (truncate *m* 2))))


(defclass h-box (box h-packing)
  ()
  (:default-initargs :x-pad 0 :y-pad 0 :gap (truncate *m* 2)))

(defmethod initialize-instance :after ((g h-box) &key label)
  (when label
    (setf (y-pad-of g) (truncate *m* 2)
          (x-pad-of g) (truncate *m* 2))))



(defclass filler (widget)
  ()
  (:default-initargs :activep nil))

(defmethod repaint ((g filler))
  (declare (ignore g))
  nil)



(defclass window (v-box sliding clipping)
  ((filler :accessor filler-of)
   (label :accessor label-of :initarg :label :initform "Untitled"))
  (:default-initargs :activep t :width 100 :height 100 :x-pad (truncate *m* 2) :y-pad (truncate *m* 3) :gap (truncate *m* 3) :pos (v 10 10)))

(defmethod initialize-instance :after ((g window) &key &allow-other-keys)
  (setf (filler-of g) (make-instance 'filler :parent g :x-expand-p t)))

(defmethod on-drag :around ((g window) start d)
  (declare (ignore d))
  (when (< (vy start) *m*)
    (call-next-method)))

(defmethod on-button-down ((g window) pos)
  (when (< (vy pos) *m*)
    (raise g)))

(defmethod repaint ((g window))
  (with-accessors ((width width-of) (height height-of) (label label-of)) g
    (draw-frame (v 0 0) width height *window-color* :style :raised)
    (draw-rectangle (v 0 0) width *m* 0 0 0 128)
    (draw-line (v 0 *m*) (v width *m*) 0 0 0 160)
    (draw-line (v 0 (1+ *m*)) (v width (1+ *m*)) 0 0 0 64)
    (draw-line (v 0 (+ *m* 2)) (v width (+ *m* 2)) 0 0 0 32)
    (with-blend (:color (color 255 255 255 255))
      (draw-text label *text-offset* *gui-font*))))



(defclass label (widget)
  ((value :reader value-of :initform "" :initarg :value)))

(defmethod initialize-instance :after ((g label) &key value &allow-other-keys)
  (when (stringp value)
    (setf (width-of g) (get-text-bounds value))))

(defmethod (setf value-of) (value (g label))
  (when (stringp value)
    (setf (width-of g) (get-text-bounds value)))
  (setf (slot-value g 'value) value))

(defmethod repaint ((g label))
  (present (value-of g) g (width-of g) (height-of g)))



(defclass pin (label sliding highlighted constrained)
  ((color :accessor color-of :initarg :color :initform *paper-color*))
  (:default-initargs :activep t))

(defmethod repaint ((g pin))
  (let ((c (color-of g)))
    (draw-rectangle (v 0 0) (width-of g) (height-of g) (color-r c) (color-g c) (color-b c) (color-a c))
    (call-next-method)
    (draw-rectangle (v 0 0) (width-of g) (height-of g) 0 0 0 (color-a c) :fill nil)))




(defclass button (widget highlighted)
  ((value :accessor value-of :initform "" :initarg :value)
   (stickyp :reader stickyp :initform nil :initarg :stickyp)
   (state :accessor state-of :initform nil :initarg :state))
  (:default-initargs :x-expand-p t))

(defmethod on-button-up ((g button) pos)
  (when (eq *armed-gob* g)
    (on-select g)
    (when (stickyp g)
      (setf (state-of g) (not (state-of g))))))

(defmethod repaint ((g button))
  (with-accessors ((width width-of) (height height-of) (value value-of)) g
    (cond
      ((or (state-of g) (armedp g))
       (draw-frame (v 0 0) width height *widget-color* :style :sunken)
       (with-transformation (:pos (v 1 1))
         (present value g width height)))
      (t
       (draw-frame (v 0 0) width height *widget-color* :style :raised)
       (present value g width height)))))





(defclass h-gauge (widget highlighted)
  ((value :accessor value-of :initarg :value :initform 0)
   (min-value :accessor min-value-of :initarg :min-value :initform 0)
   (max-value :accessor max-value-of :initarg :max-value :initform 100))
  (:default-initargs :x-expand-p t))

(defgeneric (setf value-of) (value g))
(defmethod (setf value-of) (value (g h-gauge))
  (setf (slot-value g 'value) (clamp (min-value-of g) value (max-value-of g))))


(defmethod on-drag ((g h-gauge) start-pos delta)
  (let ((x (vx (v- start-pos delta))))
    (setf (value-of g) (+ (truncate x (/ (width-of g) (abs (- (min-value-of g) (max-value-of g))))) (min-value-of g)))))

(defmethod repaint ((g h-gauge))
  (with-accessors ((width width-of) (height height-of) (value value-of) (min-value min-value-of) (max-value max-value-of)) g
    (let* ((vt (princ-to-string value))
           (sw (get-text-bounds vt))
           (k (truncate (* (/ (width-of g) (abs (- min-value max-value))) (- value min-value))))
           (kpos (v (- k (truncate sw 2)) 0)))
      (draw-frame (v 0 (truncate *m* 3)) width (truncate height 2) *window-color* :style :sunken)
      (draw-frame kpos sw *m* *widget-color* :style :raised)
      (draw-frame (v+ kpos (v (truncate sw 2) 0)) 3 (/ *m* 4) (color 0 0 0 0) :style :sunken :fill nil)
      (draw-frame (v+ kpos (v (truncate sw 2) *m*)) 3 (- (/ *m* 4)) (color 0 0 0 0) :style :sunken :fill nil)
      (with-blend (:color *text-color*)
        (draw-text vt (v+ kpos *text-offset*) *gui-font*)))))






(defclass v-slider (widget highlighted)
  ((value :accessor value-of :initarg :value :initform 0)
   (page-size :accessor page-size-of :initarg :page-size :initform 1)
   (min-value :accessor min-value-of :initarg :min-value :initform 0)
   (max-value :accessor max-value-of :initarg :max-value :initform 100))
  (:default-initargs :y-expand-p t))

(defmethod (setf value-of) (value (g v-slider))
  (setf (slot-value g 'value) (clamp (min-value-of g) value (- (max-value-of g) (funcall (page-size-of g))))))

(defmethod on-drag ((g v-slider) start-pos delta)
  (let ((y (vy (v- start-pos delta))))
    (setf (value-of g) (+ (truncate y (/ (height-of g) (abs (- (min-value-of g) (max-value-of g))))) (min-value-of g)))))

(defmethod repaint ((g v-slider))
  (with-accessors ((height height-of) (width width-of) (page-size page-size-of) (value value-of) (min-value min-value-of) (max-value max-value-of)) g
    (let* ((units (abs (- min-value max-value)))
           (ps (funcall page-size))
           (usize (/ height units))
           (k (truncate (* usize (- value min-value))))
           (kpos (v 0 k)))
      (draw-frame (v 0 0) width height *window-color* :style :sunken)
      (draw-frame kpos width
                  (min height (- height (* (- units ps) usize)))
                  *widget-color* :style :raised)
      (draw-frame (v+ kpos (v 1 (1- (truncate (min height (- height (* (- units ps) usize))) 2))))
                  (- width 2)
                  3 (color 255 255 255 0) :style :sunken))))








(defclass h-meter (widget)
  ((value :reader value-of :initarg :value :initform 0)
   (min-value :reader min-value-of :initarg :min-value :initform 0)
   (max-value :reader max-value-of :initarg :max-value :initform 100))
  (:default-initargs :activep nil :x-expand-p t))

(defmethod (setf value-of) (value (g h-meter))
  (setf (slot-value g 'value) (clamp (min-value-of g) value (max-value-of g))))

(defmethod repaint ((g h-meter))
  (with-accessors ((width width-of) (height height-of) (min-value min-value-of) (max-value max-value-of) (value value-of)) g
    (let* ( (k (truncate (* (/ width (abs (- min-value max-value))) (- value min-value)))) )
      (draw-frame (v 0 0) width height *window-color* :style :sunken)
      (loop for x from 1 to (- k 3) by 3 do
           (draw-line (v x 1) (v x (1- height)) 148 148 148 255))
      (with-blend (:color *widget-color*)
        (draw-text (princ-to-string value) (v+ (v 1 1) *text-offset*) *gui-font*))
      (with-blend (:color *text-color*)
        (draw-text (princ-to-string value) *text-offset* *gui-font*)))))




(defclass list-view (widget)
  ((items :accessor items-of :initarg :items :initform nil)
   (item-height :reader item-height-of :initarg :item-height :initform *m*)
   (multip :reader multip :initarg :multip :initform nil)
   (selected :accessor selected-of :initform nil)
   (scroll :reader scroll-of :initform 0))
  (:default-initargs :x-expand-p t :y-expand-p t))


(defgeneric (setf scroll-of) (value list-view))
(defmethod (setf scroll-of) (value (g list-view))
  (setf (slot-value g 'scroll)
        (clamp 0 value (- (* (length (items-of g)) (item-height-of g)) (height-of g)))))

(defmethod convert-selected-of ((g list-view))
  (let ((selected (mapcar (lambda (i) (nth i (items-of g))) (selected-of g))))
    (if (multip g)
        selected
        (first selected))))

(defmethod on-button-down ((g list-view) pos)
  (with-accessors ((selected selected-of) (scroll scroll-of) (item-height item-height-of)) g
    (let* ((y (vy pos))
           (item (truncate (+ y scroll) item-height)))
      (when (< item (length (items-of g)))
        (if (multip g)
            (if (find item selected :test '=)
                (setf selected (remove item selected))
                (pushnew item selected))
            (if (and selected (= (first selected) item))
                (on-select g)
                (setf selected (list item))))))))

(defmethod repaint ((g list-view))
  (with-accessors ((width width-of) (height height-of) (scroll scroll-of) (ap absolute-pos-of) (item-height item-height-of)) g
    (draw-frame (v 0 0) width height *paper-color* :style :sunken)
    (with-clipping ((vx ap) (vy ap) width height)
      (with-transformation (:pos (v 1 (- (mod scroll item-height))))
        (let ((y 0)
              (width (- width 1))
              (height (- height 1)))
          (dolist (i (items-of g))
            (when (and (> (* (1+ y) item-height) scroll)
                       (< (* y item-height) (+ scroll height)))
              (when (oddp y)
                (draw-rectangle (v 0 0) width item-height 0 0 0 32))
              (present i g width item-height)
              (when (find y (selected-of g) :test '=)
                (draw-rectangle (v 0 0) width item-height 0 0 0 128))
              (translate (v 0 item-height)))
            (incf y)))))))




(defclass list-widget (h-box)
  ((list-view :accessor list-view-of)
   (slider :accessor slider-of))
  (:default-initargs :gap 3))

(defmethod initialize-instance :after ((g list-widget) &key items (item-height *m*) (multip nil) &allow-other-keys)
  (let* ((w (truncate *m* 1.5))
         (list-view (make-instance 'list-view
                                   :multip multip
                                   :items items
                                   :item-height item-height
                                   :parent g
                                   :on-select (lambda (g)
                                                (on-select (parent-of g)))))
         (slider-box (make-instance 'v-box :parent g :gap 2 :x-expand-p nil :width w))
         (slider (make-instance 'v-slider
                                :width w
                                :parent slider-box
                                :max-value (* item-height (length items))
                                :page-size (lambda () (height-of list-view))
                                :on-drag (lambda (g pos d)
                                           (declare (ignore pos d))
                                           (setf (scroll-of list-view) (value-of g))
                                           nil))))
    (flet ((scroll-fn (d) (lambda (&rest rest)
                            (declare (ignore rest))
                            (incf (scroll-of list-view) (* d item-height))
                            (setf (value-of slider) (scroll-of list-view))
                            nil)))
      (setf (list-view-of g) list-view
            (slider-of g) slider)
      (make-instance 'button
                     :value :up-arrow
                     :parent slider-box
                     :x-expand-p nil
                     :y-expand-p nil
                     :width w
                     :height w
                     :on-button-down (scroll-fn -1)
                     :on-drag (scroll-fn -0.3))
      (make-instance 'button
                     :value :down-arrow
                     :parent slider-box
                     :x-expand-p nil
                     :y-expand-p nil
                     :width w
                     :height w
                     :on-button-down (scroll-fn 1)
                     :on-drag (scroll-fn 0.3)))))

(defmethod selected-of ((g list-widget))
  (convert-selected-of (list-view-of g)))

(defmethod items-of ((g list-widget))
  (items-of (list-view-of g)))

(defmethod (setf items-of) (items (g list-widget))
  (setf (items-of (list-view-of g)) items
        (scroll-of (list-view-of g)) 0
        (selected-of (list-view-of g)) nil
        (max-value-of (slider-of g)) (* (item-height-of (list-view-of g)) (length items))
        (value-of (slider-of g)) 0))




(defclass radio-item (button)
  ())

(defmethod repaint ((g radio-item))
  (with-accessors ((height height-of) (width width-of) (value value-of)) g
    (let* ((m/2 (truncate *m* 2))
           (m/4 (truncate m/2 2))
           (ypos (truncate height 2)))
      (draw-circle (v m/4 ypos)
                   (1+ (truncate m/2 2))
                   0 0 0 255
                   :smoothp t :segments 10)
      (draw-circle (v m/4 ypos)
                   (truncate m/2 2)
                   (color-r *paper-color*) (color-g *paper-color*) (color-b *paper-color*) (color-a *paper-color*)
                   :smoothp t :segments 10)
      (when (state-of g)
        (draw-circle (v m/4 ypos) (- (truncate m/2 2) 2)
                     0 0 0 255
                     :smoothp t :segments 10))
      (with-transformation (:pos (v (truncate *m* 1.5) 0))
        (present value g (- width *m*) height)))))


(defclass choice-item (button)
  ())

(defmethod repaint ((g choice-item))
  (with-accessors ((height height-of) (width width-of) (value value-of)) g
    (let* ((m/2 (truncate *m* 2))
           (ypos (- (truncate height 2) (truncate m/2 2))))
      (draw-frame (v 0 ypos)
                  m/2 m/2
                  *paper-color*
                  :style :sunken)
      (when (state-of g)
        (draw-frame (v 1 (- ypos -1))
                    (- m/2 1) (- m/2 1)
                    *widget-color*
                    :style :raised))
      (with-transformation (:pos (v (truncate *m* 1.5) 0))
        (present value g (- width *m*) height)))))




(defclass choice-widget (v-box)
  ((multip :accessor multip :initarg :multip :initform nil)
   (items :accessor items-of :initarg :items :initform nil)))

(defmethod initialize-instance :after ((g choice-widget) &key items multip (item-height *m*) &allow-other-keys)
  (setf (items-of g) (mapcar (lambda (i)
                               (make-instance (if multip 'choice-item 'radio-item)
                                              :parent g
                                              :height item-height
                                              :value i
                                              :stickyp t
                                              :on-select (lambda (c)
                                                           (declare (ignore c))
                                                           (unless multip
                                                             (dolist (c (childs-of g))
                                                               (setf (state-of c) nil)))
                                                           (on-select g)
                                                           nil)))
                             items))
  (unless multip
    (setf (selected-of g) (first items))))

(defmethod selected-of ((g choice-widget))
  (if (multip g)
      (mapcar 'value-of (remove-if-not 'state-of (childs-of g)))
      (first (mapcar 'value-of (remove-if-not 'state-of (childs-of g))))))

(defmethod (setf selected-of) (object (g choice-widget))
  (setf (state-of (find object (childs-of g) :key 'value-of)) t))




(defclass text-widget (widget)
  ((point :accessor point-of :initform 0)
   (text :accessor text-of :initarg :text :initform ""))
  (:default-initargs :x-expand-p t))

(defmethod initialize-instance :after ((g text-widget) &key text &allow-other-keys)
  (setf (point-of g) (length text)))

(defmethod repaint ((g text-widget))
  (with-accessors ((width width-of) (height height-of) (text text-of) (point point-of)) g
    (draw-frame (v 0 0) width height *widget-color* :fill nil :style :sunken)
    (draw-rectangle (v 1 1) (1- width) (1- height) (color-r *paper-color*) (color-g *paper-color*) (color-b *paper-color*) (color-a *paper-color*))
    (let ( (point-x (+ (vx *text-offset*) (get-text-size (subseq text 0 point)))))
      (with-blend (:color *text-color*)
        (draw-text text *text-offset* *gui-font*)
        (when (focusedp g)
          (draw-rectangle (v point-x (vy *text-offset*))
                          2 (- height (* 2 (vy *text-offset*)))
                          0 0 0 255))))))

(defmethod on-key-down ((g text-widget) char)
  (setf (text-of g) (concatenate 'string (text-of g) (string char)))
  (incf (point-of g)))




(defclass tooltip (gob)
  ((host :accessor host-of :initarg :host)
   (text :reader text-of :initarg :text :initform ""))
  (:default-initargs :activep nil :width 100 :height *m* :pos (get-mouse-pos)))

(defmethod initialize-instance :after ((g tooltip) &key text &allow-other-keys)
  (setf (width-of g) (get-text-bounds text))
  (raise g))


(defmethod repaint ((g tooltip))
  (unless (pointedp (host-of g))
    (setf (parent-of g) nil))
  (draw-rectangle (v 0 0) (width-of g) (height-of g) (color-r *paper-color*) (color-g *paper-color*) (color-b *paper-color*) (color-a *paper-color*))
  (draw-rectangle (v 0 0) (width-of g) (height-of g) 0 0 0 255 :fill nil)
  (with-blend (:color *text-color*)
    (draw-text (text-of g) *text-offset* *gui-font*)))