(defpackage :test
  (:use :cl :pal :pal-gui))
(in-package :test)


(defun test ()
  (with-pal ()
    (let* ((window (make-instance 'window :pos (v 200 200) :width 300 :height 240))

           (hbox (make-instance 'h-box :parent window))
           (left-box (make-instance 'v-box :parent hbox :label "Left"))
           (right-box (make-instance 'v-box :parent hbox :label "Right"))
           (bottom-box (make-instance 'v-box :parent window :label "Bottom" :y-expand-p nil)))

      (let ((a (make-instance 'button :value "Button" :parent right-box))
            (b (make-instance 'button :value "Button" :parent right-box))
            (c (make-instance 'button :value "Button" :parent right-box))
            (d (make-instance 'button :value "Button" :parent right-box))
            (e (make-instance 'button :value "Button" :parent bottom-box))
            (f (make-instance 'button :value "a Button" :parent left-box)))

        (event-loop ()
          (clear-screen (color 50 50 255)))))))

;; (test)


(defun test ()
  (with-pal ()
    (let* ((window (make-instance 'window :pos (v 200 200) :width 300 :height 200)))

      (let ((a (make-instance 'button :value "Button" :parent window :y-expand-p t))
            (b (make-instance 'button :value "Button" :parent window))
            (c (make-instance 'button :value "Foo" :parent window :y-expand-p t)))

        (event-loop ()
          (clear-screen (color 50 50 255)))))))

;; (test)



(defun test ()
  (with-pal ()
    (let* ((window (make-instance 'window :pos (v 200 200) :width 300 :height 200)))
      (let* ((hbox (make-instance 'h-box :parent window))
             (box (make-instance 'box :label "Box" :parent window))
             (pin (make-instance 'pin :value "Foo" :color (color 255 30 30 128) :parent box :pos (v 100 30)))
             (a (make-instance 'button :value "Button" :parent hbox))
             (f (make-instance 'filler :parent hbox))
             (b (make-instance 'button :value "Button" :parent hbox))
             (vbox (make-instance 'v-box :label "foo" :parent hbox :width 30 :x-expand-p nil))
             (c (make-instance 'button :value "Foo" :parent vbox)))

        (loop repeat 10 do (make-instance 'pin :value "Foo" :b 0 :pos (v (random 800) (random 600))))

        (event-loop ()
          (clear-screen (color 50 50 255)))))))

;; (test)