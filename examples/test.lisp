;; TODO:
;;
;; window sizing, dialogs, menus, keyboard control, scrollwheel
;; debugging utils, scrolling mixin
;; scroll box, paragraph, text box, simple editor, combo box, tree view, gridbox, property list, tabs
;; File open/save, choose directory, yes/no dialogs, color selector

(defpackage :test
  (:use :cl :pal :pal-gui))
(in-package :test)


(defun test ()
  (with-pal (:paths (list "C:/Documents and Settings/tomppa/Omat tiedostot/" (merge-pathnames "examples/" pal::*pal-directory*)))
    (let* ((plane (load-image "lego-plane.png"))
           (tile (load-image "ground.png"))

           (window (make-instance 'window :pos (v 480 200) :width 300 :height 240))
           (window-2 (make-instance 'window :width 200 :height 300))

           (box (make-instance 'h-box :parent window))
           (left-box (make-instance 'v-box :parent box :label "RGBA"))
           (right-box (make-instance 'v-box :parent box :label "Current FPS"))
           (bottom-box (make-instance 'v-box :parent window :label "Bar" :y-expand-p nil))

           (meter (make-instance 'h-meter :parent right-box :max-value 100 :on-repaint (lambda (g) (setf (value-of g) (get-fps)) nil)))
           (multichoice (make-instance 'choice-widget :multip t :parent right-box :items '(Foo Bar Baz)))
           (rg (make-instance 'h-gauge :parent left-box
                                       :min-value 0 :max-value 255 :value 0))
           (gg (make-instance 'h-gauge :parent left-box
                                       :min-value 0 :max-value 255 :value 0))
           (bg (make-instance 'h-gauge :parent left-box
                                       :min-value 0 :max-value 255 :value 0))
           (ag (make-instance 'h-gauge :parent left-box
                                       :min-value 0 :max-value 255 :value 0))
           (list (make-instance 'list-widget :parent window-2
                                             :item-height 48
                                             :items (loop for i from 0 to 50 collect (format nil "FooBar ~a" i))
                                             :multip nil
                                             :on-select (lambda (g)
                                                          (message (selected-of g)))))
           (button (make-instance 'button :value :circle
                                                 :tooltip "Push me to change the listview"
                                                 :parent window-2
                                                 :on-select (lambda (g) (setf (items-of list) (remove-if-not 'image-p pal-ffi::*resources*)))))
           (choice (make-instance 'choice-widget :label "Foo" :parent window-2 :items '("First" "Second" "and Third")))
           (pin (make-instance 'pin :value "Plane" :pos (v 400 300)))
           (text (make-instance 'text-widget :text "Text" :parent bottom-box)))

      (event-loop ()
                  (draw-image* tile (v 0 0) (v 0 0) 800 600)
                  (with-blend (:color (color 0 0 0 64))
                    (draw-image plane (pos-of pin)))
                  (with-blend (:color (color (value-of rg) (value-of gg) (value-of bg) (value-of ag)))
                    (draw-image plane (v- (pos-of pin) (v 10 10))))))))

;; (test)