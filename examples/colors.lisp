(in-package :pal-gui)


(defparameter *bg* (color 0 0 0))

(defmethod present ((c color) w width height)
  (with-blend (:color c)
    (draw-text (format nil "#~16R~16R~16R" (color-r c) (color-g c) (color-b c)) *text-offset*)))

(defmethod present ((c color) (w list-view) width height)
  (draw-rectangle (v 0 0) width height (color-r c) (color-g c) (color-b c) 255))


(defun test ()
  (with-pal (:paths (merge-pathnames "examples/" pal::*pal-directory*))
    (let* ((window (make-instance 'window :pos (v 200 200) :width 200 :height 230 :label "Select color"))
           (button (make-instance 'button :value ""
                                          :parent window))
           (list (make-instance 'list-widget :parent window
                                             :on-select (lambda (g)
                                                          (setf (value-of button) (selected-of g)))
                                             :items (loop repeat 100 collecting (random-color)))))

      (setf (on-select-of button) (lambda (g)
                                    (setf *bg* (selected-of list))))

      (event-loop ()
        (clear-screen *bg*)))))

;; (test)