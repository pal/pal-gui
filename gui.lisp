(in-package :pal-gui)

(declaim (optimize (speed 3)))

(defvar *update-screen-fn* (symbol-function 'pal:update-screen))
(defvar *open-pal-fn* (symbol-function 'pal:open-pal))



(defun config-gui (&key (font *gui-font*) (window-color *window-color*) (widget-color *widget-color*)
                   (paper-color *paper-color*) (tooltip-delay *tooltip-delay*) (text-color *text-color*))
  (setf *gui-font* font
        *window-color* window-color
        *widget-color* widget-color
        *text-color* text-color
        *paper-color* paper-color
        *tooltip-delay* tooltip-delay
        *m* (truncate (* (get-font-height *gui-font*) 1.5))
        *text-offset* (let ((fh (get-font-height *gui-font*)))
                        (v (truncate fh 2) (truncate fh 4)))))

(defun pal:update-screen ()
  "Like PAL:UPDATE but also updates the GUI"
  (pal::close-quads)
  (reset-blend)
  (pal-ffi:gl-load-identity)
  (repaint *root*)
  (funcall *update-screen-fn*))


(defun active-gobs-at-point (point parent)
  (let ((c (find-if (lambda (c)
                      (point-inside-p c point))
                    (reverse (childs-of parent)))))
    (if c
        (if (activep c)
            (cons c (active-gobs-at-point point c))
            (active-gobs-at-point point c))
        nil)))

(defun init-gui ()
  (setf *root* (make-instance 'root :parent nil)
        *gui-font* (tag 'pal::default-font)
        *drag-start-pos* nil
        *relative-drag-start-pos* nil
        *focused-gob* nil
        *pointed-gob* nil
        *armed-gob* nil)
  (config-gui :font (tag 'pal::default-font)
              :window-color (color 128 128 128 220)
              :widget-color (color 160 160 160 220)
              :text-color (color 0 0 0 255)
              :paper-color (color 255 255 200 255)
              :tooltip-delay 1))



(defmacro pal:event-loop ((&key key-up-fn key-down-fn mouse-motion-fn quit-fn) &body redraw)
  "Same as PAL:EVENT-LOOP but with added GUI event handling"
  (let ((event (gensym)))
    `(block event-loop
       (cffi:with-foreign-object (,event :char 500)
         (let ((key-up (lambda (key)
                         (case key
                           (:key-mouse-1 (cond
                                           (*pointed-gob*
                                            (on-button-up *pointed-gob* (v- (get-mouse-pos) (absolute-pos-of *pointed-gob*))))
                                           (t (pal::funcall? ,key-up-fn key)))
                                         (setf *armed-gob* nil))
                           (otherwise (pal::funcall? ,key-up-fn key)))))
               (key-down (lambda (key)
                           (case key
                             (:key-mouse-2 (when *pointed-gob*
                                             (on-inspect *pointed-gob*)))
                             (:key-escape (unless ,key-down-fn
                                            (return-from event-loop)))
                             (:key-mouse-1 (cond
                                             (*pointed-gob*
                                              (setf *drag-start-pos* (get-mouse-pos)
                                                    *relative-drag-start-pos* (v- *drag-start-pos* (absolute-pos-of *pointed-gob*))
                                                    *focused-gob* *pointed-gob*
                                                    *armed-gob* *pointed-gob*)
                                              (on-button-down *pointed-gob* (v- (get-mouse-pos) (absolute-pos-of *pointed-gob*))))
                                             (t (setf *focused-gob* nil)
                                                (pal::funcall? ,key-down-fn key))))
                             (otherwise (if *focused-gob*
                                            (let ((char (keysym-char key)))
                                              (when (and char (graphic-char-p char)) (on-key-down *focused-gob* char)))
                                            (pal::funcall? ,key-down-fn key)))))))

           (loop
              (pal::do-event ,event key-up key-down ,mouse-motion-fn ,quit-fn)
              ,@redraw
              (let ((g (first (last (active-gobs-at-point (get-mouse-pos) *root*)))))
                (cond
                  (*armed-gob*
                   (on-drag *armed-gob* *relative-drag-start-pos* (v- *drag-start-pos* (get-mouse-pos))))
                  ((and g (not (eq g *pointed-gob*)))
                   (on-enter g)))
                (when g
                  (on-over g))
                (when (and *pointed-gob* (not (eq *pointed-gob* g)))
                  (on-leave *pointed-gob*))
                (setf *pointed-gob* g))
              (update-screen)))))))

(defun pal:open-pal (&rest args)
  (apply *open-pal-fn* args)
  (init-gui))
