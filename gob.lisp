(in-package :pal-gui)

(declaim (optimize (speed 3)))


(defvar *root* nil)
(defvar *drag-start-pos* nil)
(defvar *relative-drag-start-pos* nil)
(defvar *focused-gob* nil)
(defvar *pointed-gob* nil)
(defvar *armed-gob* nil)



(defclass gob ()
  ((pos :accessor pos-of :initarg :pos :initform (v 0 0))
   (parent :reader parent-of :initform nil)
   (x-expand-p :accessor x-expand-p :initform nil :initarg :x-expand-p)
   (y-expand-p :accessor y-expand-p :initform nil :initarg :y-expand-p)
   (activep :accessor activep :initform t :initarg :activep)
   (width :accessor width-of :initarg :width :initform 0)
   (height :accessor height-of :initarg :height :initform 0)
   (min-width :reader min-width-of :initarg :min-width)
   (min-height :reader min-height-of :initarg :min-height)
   (childs :reader childs-of :initform nil)))

(defmethod initialize-instance :after ((g gob) &key (min-height) (min-width) (parent *root*) &allow-other-keys)
  (unless min-width
    (setf (slot-value g 'min-width) (width-of g)))
  (unless min-height
    (setf (slot-value g 'min-height) (height-of g)))
  (setf (parent-of g) parent))

(defgeneric repaint (gob))
(defmethod repaint :around ((g gob))
  (with-transformation (:pos (pos-of g))
    (call-next-method)
    (repaint-childs g)))

(defgeneric lower (gob))
(defmethod lower ((g gob))
  (setf (slot-value (parent-of g) 'childs)
        (cons g (remove g (childs-of (parent-of g))))))

(defgeneric raise (gob))
(defmethod raise ((g gob))
  (setf (slot-value (parent-of g) 'childs)
        (append (remove g (childs-of (parent-of g))) (list g))))

(defgeneric absolute-pos-of (gob))
(defmethod absolute-pos-of ((g gob))
  (if (parent-of g)
      (v+ (pos-of g) (absolute-pos-of (parent-of g)))
      (pos-of g)))

(defgeneric (setf absolute-pos-of) (pos gob))
(defmethod (setf absolute-pos-of) (pos (g gob))
  (setf (pos-of g) (v+ (v- pos (absolute-pos-of g)) (pos-of g))))

(defgeneric point-inside-p (gob point))
(defmethod point-inside-p ((g gob) point)
  (point-inside-rectangle-p (absolute-pos-of g) (width-of g) (height-of g) point))


(defgeneric on-inspect (gob))
(defmethod on-inspect ((g gob))
  nil)

(defgeneric on-over (gob))
(defmethod on-over ((gob gob))
  nil)

(defgeneric on-enter (gob))
(defmethod on-enter ((gob gob))
  nil)

(defgeneric on-leave (gob))
(defmethod on-leave ((gob gob))
  nil)

(defgeneric on-button-down (gob pos))
(defmethod on-button-down ((gob gob) pos)
  nil)

(defgeneric on-button-up (gob pos))
(defmethod on-button-up ((gob gob) pos)
  nil)

(defgeneric on-select (gob))
(defmethod on-select ((gob gob))
  nil)

(defgeneric on-key-down (gob char))
(defmethod on-key-down ((gob gob) char)
  nil)

(defgeneric on-drag (gob start-pos delta-pos))
(defmethod on-drag ((gob gob) start-pos delta)
  (declare (ignore start-pos delta))
  nil)

(defgeneric pointedp (gob))
(defmethod pointedp ((gob gob))
  (eq *pointed-gob* gob))

(defgeneric focusedp (gob))
(defmethod focusedp ((gob gob))
  (eq *focused-gob* gob))

(defgeneric armedp (gob))
(defmethod armedp ((gob gob))
  (eq *armed-gob* gob))




(defgeneric pack (gob))
(defmethod pack ((g gob))
  (declare (ignore g))
  nil)

(defgeneric repaint-childs (gob))
(defmethod repaint-childs ((g gob))
  (dolist (c (childs-of g))
    (repaint c)))

(defgeneric adopt (parent child))
(defmethod adopt ((parent gob) (child gob))
  (when (parent-of child)
    (abandon (parent-of child) child))
  (setf (slot-value child 'parent) parent)
  (push child (slot-value parent 'childs)))

(defgeneric abandon (parent child))
(defmethod abandon ((parent gob) (child gob))
  (setf (slot-value parent 'childs) (remove child (slot-value parent 'childs))
        (slot-value child 'parent) nil))

(defgeneric abandon-all (parent))
(defmethod abandon-all ((parent gob))
  (dolist (c (childs-of parent))
    (abandon parent c)))

(defgeneric (setf childs-of) (childs parent))
(defmethod (setf childs-of) (childs (parent gob))
  (dolist (c childs)
    (adopt parent c)))


(defgeneric (setf parent-of) (parent child))
(defmethod (setf parent-of) (parent (child gob))
  (when (parent-of child)
    (abandon (parent-of child) child))
  (when parent
    (adopt parent child)))

(defmethod (setf width-of) (width (g gob))
  (when (/= (slot-value g 'width) width)
    (setf (slot-value g 'width) width)
    (pack g))
  (setf (slot-value g 'width) width))

(defmethod (setf height-of) (height (g gob))
  (when (/= (slot-value g 'height) height)
    (setf (slot-value g 'height) height)
    (pack g))
  (setf (slot-value g 'height) height))






(defclass v-packing (gob)
  ((x-pad :accessor x-pad-of :initarg :x-pad :initform 0)
   (y-pad :accessor y-pad-of :initarg :y-pad :initform 0)
   (gap :accessor gap-of :initarg :gap :initform 0)))

(defmethod adopt ((parent v-packing) (child gob))
  (call-next-method)
  (pack parent))

(defmethod abandon ((parent v-packing) (child gob))
  (call-next-method)
  (pack parent))

(defmethod min-width-of ((g v-packing))
  (let ((childs-min (loop for c in (childs-of g) maximizing (min-width-of c))))
    (+ (if childs-min childs-min 0)
       (gap-of g)
       (* 2 (x-pad-of g)))))

(defmethod min-height-of ((g v-packing))
  (+ (* (1- (length (childs-of g))) (gap-of g))
     (* 2 (y-pad-of g))
     (loop for c in (childs-of g) summing (min-height-of c))))

(defmethod pack ((g v-packing))
  (with-accessors ((gap gap-of) (width width-of) (min-height min-height-of) (height height-of) (pos pos-of)
                   (parent parent-of) (childs childs-of) (y-pad y-pad-of) (x-pad x-pad-of)) g
    (let* ((exp-count (count-if #'y-expand-p childs))
           (exp-size (- height (+ (* (1- (length (childs-of g))) (gap-of g)) (* 2 (y-pad-of g))
                                  (loop for c in (remove-if 'y-expand-p (childs-of g)) summing (min-height-of c))))))
      (dolist (c childs)
        (setf (height-of c)
              (if (y-expand-p c)
                  (max (min-height-of c) (truncate exp-size exp-count))
                  (min-height-of c)))
        (setf (width-of c)
              (if (x-expand-p c)
                  (max 1 (- width (* 2 x-pad)))
                  (min-width-of c)))))
    (let ((cpos (v x-pad y-pad)))
      (dolist (c (reverse childs))
        (setf (pos-of c) cpos)
        (setf cpos (v+ cpos (v 0 (+ gap (height-of c)))))))
    (pack parent)))




(defclass h-packing (v-packing)
  ())

(defmethod min-height-of ((g h-packing))
  (let ((childs-min (loop for c in (childs-of g) maximizing (min-height-of c))))
    (+ (if childs-min childs-min 0)
       (gap-of g)
       (* 2 (y-pad-of g)))))

(defmethod min-width-of ((g h-packing))
  (+ (* (1- (length (childs-of g))) (gap-of g))
     (* 2 (x-pad-of g))
     (loop for c in (childs-of g) summing (min-width-of c))))

(defmethod pack ((g h-packing))
  (with-accessors ((gap gap-of) (height height-of) (min-width min-width-of) (width width-of) (pos pos-of)
                   (parent parent-of) (childs childs-of) (y-pad y-pad-of) (x-pad x-pad-of)) g
    (let* ((exp-count (count-if #'x-expand-p childs))
           (exp-size (- width (+ (* (1- (length (childs-of g))) (gap-of g)) (* 2 (x-pad-of g))
                                 (loop for c in (remove-if 'x-expand-p (childs-of g)) summing (min-width-of c))))))
      (dolist (c childs)
        (setf (width-of c)
              (if (x-expand-p c)
                  (max (min-width-of c) (truncate exp-size exp-count))
                  (min-width-of c)))
        (setf (height-of c)
              (if (y-expand-p c)
                  (max 1 (- height (* 2 y-pad)))
                  (min-height-of c)))))
    (let ((cpos (v x-pad y-pad)))
      (dolist (c (reverse childs))
        (setf (pos-of c) cpos)
        (setf cpos (v+ cpos (v (+ gap (width-of c)) 0)))))
    (pack parent)))






(defclass sliding ()
  ((start-pos :accessor start-pos-of)))


(defmethod on-button-down :around ((g sliding) pos)
  (declare (ignore pos))
  (setf (start-pos-of g) (pos-of g))
  (call-next-method))

(defmethod on-drag :around ((g sliding) start-pos delta)
  (declare (ignore start-pos))
  (setf (pos-of g) (v- (start-pos-of g) delta))
  (call-next-method))



(defclass clipping ()
  ())

(defmethod repaint-childs :around ((g clipping))
  (let ((ap (absolute-pos-of g)))
    (with-clipping ((1+ (vx ap)) (1+ (vy ap)) (- (width-of g) 2) (- (height-of g) 2))
      (call-next-method))))




(defclass highlighted ()
  ())

(defgeneric highlight (g))

(defmethod repaint :after ((g highlighted))
  (when (and (or (not *armed-gob*) (eq g *armed-gob*)) (and (activep g) (pointedp g)))
    (highlight g)))



(defclass constrained ()
  ())

(defmethod (setf pos-of) :around (pos (g constrained))
  (call-next-method)
  (constrain g))

(defmethod (setf width-of) :around (width (g constrained))
  (call-next-method)
  (constrain g))

(defmethod (setf height-of) :around (height (g constrained))
  (call-next-method)
  (constrain g))

(defmethod constrain ((g constrained))
  (with-accessors ((pos pos-of) (width width-of) (height height-of) (parent parent-of)) g
    (setf (slot-value g 'pos) (v (clamp 0 (vx pos) (- (width-of parent) width))
                                 (clamp 0 (vy pos) (- (height-of parent) height))))))




(defclass root (gob)
  ()
  (:default-initargs :width (get-screen-width) :height (get-screen-height) :pos (v 0 0) :parent nil))

(defmethod repaint ((g root))
  (declare (ignore g))
  nil)

(defmethod (setf parent-of) (parent (root root))
  (declare (ignore parent))
  nil)